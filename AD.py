from math import log
from comum import Amostra
from comum import No
from copy import copy
from copy import deepcopy

def agrupa(n):
	if 0 <= n <= 31:
		return 0
	elif 32 <= n <= 63:
		return 1
	elif 64 <= n <= 95:
		return 2
	elif 96 <= n <= 127:
		return 3
	elif 128 <= n <= 159:
		return 4
	elif 160 <= n <= 191:
		return 5
	elif 192 <= n <= 223:
		return 6
	elif 224 <= n <= 255:
		return 7


POSSIVEIS =   { 
	'c1' : range(8),
	'c2' : range(8),
	'c3' : range(8) }

ATRIBUTOS = sorted(POSSIVEIS.keys())

def filtrar(atributo, procurado, cnj):
	return filter(lambda w: agrupa(w.__getattribute__(atributo)) == procurado, cnj)

def verifica_situacao1(conjunto):
	return len(conjunto) == 0

def verifica_situacao2(conjunto):
	return len(conjunto) * [conjunto[0].rotulo] == map(lambda x: x.rotulo, conjunto)

def verifica_situacao3(conjunto):
	return all([i.c1 == i.c2 == i.c3 == None for i in conjunto])

def checagem(conjunto, conjunto_nivel_acima):
	if verifica_situacao1(conjunto):
		return acao_situacao_1(conjunto_nivel_acima)
	elif verifica_situacao2(conjunto):
		return acao_situacao_2(conjunto)
	elif verifica_situacao3(conjunto):
		return acao_situacao_3(conjunto)
	else:
		return acao_situacao_4(conjunto)

def acao_situacao_1(cnj_acima):
	P = 1, map(lambda y: y.rotulo, cnj_acima).count(1)
	N = 2, map(lambda y: y.rotulo, cnj_acima).count(2)
	return max((P,N),key=lambda x: x[1])[0]

def acao_situacao_2(cnj):
	return cnj[0].rotulo

def acao_situacao_3(cnj):
	P = 1, map(lambda y: y.rotulo, cnj).count(1)
	N = 2, map(lambda y: y.rotulo, cnj).count(2)
	return max((P,N),key=lambda x: x[1])[0]

def acao_situacao_4(cnj):
	ganhos = [(atributo, ganho(atributo, cnj)) for atributo in ATRIBUTOS]
	return max(ganhos, key=lambda x: x[1])[0]

def ganho(atributo, cnj):
	return informacao(cnj) - entropia(atributo, cnj)

def I(x, y):
	if x == 0 and y == 0:
		return 0
	elif x == 0:
		return - ((y)*(log(y, 2)))
	elif y == 0:
		return ((-1)*(x)*(log(x, 2)))
	return ((-1)*(x)*(log(x, 2))) - ((y)*(log(y, 2)))

def informacao(cnj):
	P = float(map(lambda y: y.rotulo, cnj).count(1))
	N = float(map(lambda y: y.rotulo, cnj).count(2))
	ppn = P/(P+N)
	npn = N/(P+N)
	return I(ppn, npn)

def entropia(atributo, cnj):
	return sum([R(atributo, i, cnj) for i in POSSIVEIS[atributo]])

def R(atributo, classe, cnj):
	P = float(map(lambda y: y.rotulo, cnj).count(1))
	N = float(map(lambda y: y.rotulo, cnj).count(2))
	cnj_filtrado = filtrar(atributo, classe, cnj)
	p = float(map(lambda y: y.rotulo, cnj_filtrado).count(1))
	n = float(map(lambda y: y.rotulo, cnj_filtrado).count(2))

	try :
		x = ((p + n)/(P+N)) * I(p/(p+n), n/(p+n))
	except ZeroDivisionError:
		x = 0

	return x

def constroi(cnj_treinamento):
	no = No()
 	no.rotulo = checagem(cnj_treinamento, None)
 	no.conjunto = deepcopy(cnj_treinamento)
	raiz = copy(no)
 	pilha = []

 	if not no.e_folha():
 		for possivel in POSSIVEIS[no.rotulo]:
 			no2 = No()
 			filtrado = filtrar(no.rotulo, possivel, cnj_treinamento)
 			no2.rotulo = checagem(filtrado, no.conjunto)
 			no2.conjunto = deepcopy(filtrado)
 			no2.limpa_atributo(no.rotulo)
 			no.filhos.append(no2)
 			pilha.append(no2)

	while pilha:
 		no = pilha.pop()
 		if not no.e_folha():
		 	for possivel in POSSIVEIS[no.rotulo]:
	 			no2 = No()
 				filtrado = filtrar(no.rotulo, possivel, no.conjunto)
 				no2.rotulo = checagem(filtrado, no.conjunto)
 				no2.conjunto = deepcopy(filtrado)
 				no2.limpa_atributo(no.rotulo)
	 			no.filhos.append(no2)
	 			pilha.append(no2)
	return raiz

def classifica(instancia_n_rotulada, raiz):
	no = raiz
	if raiz.e_folha():
		return raiz.rotulo

	for atributo in ATRIBUTOS:
		no = no.filhos[POSSIVEIS[no.rotulo].index(agrupa(instancia_n_rotulada.__getattribute__(no.rotulo)))]
		if no.e_folha():
			return no.rotulo