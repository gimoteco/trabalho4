#-*- coding: utf-8 -*-

# Importações
from comum import Amostra

# Constantes de código
BIAS = -1
TAXA_DE_APRENDIZAGEM = 0.2
PESO_INICIAL = 0.2
NUMERO_DE_ENTRADAS = 3

def funcao_limiar(valor):
    ''' Função limiar retorna + se > 0 senão - '''
    if valor > 0:
        return 2
    return 1

def somatorio(entrada, pesos):
    ''' Função que calcula qual a soma das multiplicações entre cada elemento de entrada com pesos '''
    obtido = 0.0
    for i, j in zip(entrada, pesos):
        obtido += i*j
    return obtido


def treinar(conjunto_de_treinamento, numero_de_epocas):
    ''' Função de treinamento do Perceptron '''

    # Cria uma lista com os pesos das conexoes (+3 porque são 3 entradas, +1 por causa do BIAS)
    pesos = [PESO_INICIAL] * (NUMERO_DE_ENTRADAS + 1)

    # Treinamento
    # Para cada época
    for epoca in range(numero_de_epocas):
        # Para cada instância conhecida
        for instancia_rotulada in conjunto_de_treinamento:
            # Transforma em float
            entrada = (instancia_rotulada.c1, instancia_rotulada.c2, instancia_rotulada.c3, -1)
            esperado = instancia_rotulada.rotulo

            entrada = map(lambda x: float(x), entrada)

            # Calcula a soma dos pesos com a entrada
            obtido = somatorio(entrada, pesos)

            erro = esperado - funcao_limiar(obtido)

            for i, valor_aresta in enumerate(pesos):
                pesos[i] = valor_aresta + (erro * TAXA_DE_APRENDIZAGEM * entrada[i])

    # Retorna os novos pesos após o treinamento.
    return pesos
    

def classificar(instancia_n_rotulada, pesos):
    ''' Função que classifica uma instância usando a função limiar '''

    entrada = (instancia_n_rotulada.c1, instancia_n_rotulada.c2, instancia_n_rotulada.c3, -1)
    obtido = somatorio(entrada, pesos)
    return funcao_limiar(obtido)