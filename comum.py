#-*- coding: utf-8 -*-
import math

def distancia_euclidiana(args1, args2):
	'''
		Função que calcula a distancia euclidiana entre duas de argumentos (args1, args2) 
		... sqrt((args1[0] - args2[0])^2 + ...)
	'''
	if len(args1) == len(args2):
		return math.sqrt(sum(map(lambda x: pow(x[0] - x[1], 2), zip(args1, args2))))
	else :
		raise ValueError

class Amostra(object):
	'''
	Classe para armazenar a instancia
	'''

	def __init__(self, c1, c2, c3, rotulo):
		self.c1 = c1
		self.c2 = c2
		self.c3 = c3
		self.rotulo = rotulo

	def __repr__(self):
		return '<{self.c1}, {self.c2}, {self.c3}>'.format(self=self)

class No(object):

	def __init__(self, rotulo=None, conjunto=None):
		self.rotulo = rotulo
		self.conjunto = conjunto
		self.filhos = []

	def limpa_atributo(self, atributo):
		for ele in self.conjunto:
			ele.__setattr__(atributo, None)

	def e_folha(self):
		return self.rotulo == 1 or self.rotulo == 2

	def __repr__(self):
		return '<{} {}>'.format(self.rotulo, self.filhos)

class Vertice(object):

	def __init__(self, rotulo=None, filhos=[]):
		self.rotulo = rotulo
		self.filhos = filhos

	def e_folha(self):
		return self.rotulo == 's' or self.rotulo == 'n'

	def __repr__(self):
		return '<{} {}>'.format(self.rotulo, self.filhos)