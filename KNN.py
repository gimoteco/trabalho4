#-*- coding: utf-8 -*-
from comum import Amostra
from comum import distancia_euclidiana

def KNN(instancia_nao_rotulada, k, conjunto_de_treinamento):
	'''
	Função que recebe uma instancia não rotulada, um k e um conjunto
	de treinamento, para classificar a instancia não rotulada, usando
	o algoritmo KNN.
	'''

	# Inicialização da lista de ranking
	ranking = []
	# Campos usados na distancia euclidiana
	attr_n_rotulada = [
		instancia_nao_rotulada.c1,
		instancia_nao_rotulada.c2,
		instancia_nao_rotulada.c3
	]

	# Calcula todas os rankings
	for instancia in conjunto_de_treinamento:		
		attr_instancia = [instancia.c1, instancia.c2, instancia.c3]
		ranking.append((instancia, distancia_euclidiana(attr_instancia, attr_n_rotulada)))

	# Ordena, e pega o K-éssimo maior valor de ranking
	ranking.sort(key=lambda x: x[1])
	kessimo_maior_rank = ranking[k-1][1]

	# Novo conjunto somente com os melhores
	novo_conjunto = []
	for instancia, rank in ranking:
		if rank <= kessimo_maior_rank:
			novo_conjunto.append(instancia)

	# Criar contadores para contar rótulos
	grupos = {1:0, 2:0}

	# Percore todas as instancias do novo conjunto (só os top), contando se é + ou -.
	for instancia in novo_conjunto:
		if instancia.rotulo == 1:
			grupos[1]+=1
		else:
			grupos[2]+=1

	# Caso do empate e vitoria do +
	if grupos[1] >= grupos[2]:
		return 1
	else:
		return 2