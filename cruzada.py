#-*- coding: utf-8 -*-
# Importações
from copy import copy
from comum import Amostra
import Perceptron
import KNN
import AD

# Constantes de código
NUMERO_DE_DOBRAS = 10
NUMERO_DE_EPOCAS_DO_PERCEPTRON = 100
K_DO_KNN = 100

def validacao_cruzada(tipo_de_busca, conjunto_de_treinamento, k):
	'''
		Função que implementa a validação cruzada, divide em k grupos
		de (len(conjunto_de_treinamento)/k) elementos, cada iteração 
		muda o conjutno de treinamento, evitando assim 
	'''
	# Divide os grupos
	grupos = [conjunto_de_treinamento[i:i+(len(conjunto_de_treinamento)/k)] for i in range(0, len(conjunto_de_treinamento), len(conjunto_de_treinamento)/k)]

	# Inicializa a matriz de confusão
	mat_confusao = {1:{1:0, 2:0}, 2:{1:0, 2:0}}

	# Itera sobre os os conjuntos divididos
	for i_conjunto, conjunto_escolhido in enumerate(grupos):
		# Os demais grupos exceto o escolhido compõem o cj. de treinamento
		grupos_de_treinamento = copy(grupos)
		del grupos_de_treinamento[i_conjunto]

		#Expande grupos ((1), (1)) vira (1,1)
		conjunto_de_treinamento_expandido = []
		for i in grupos_de_treinamento:
			conjunto_de_treinamento_expandido.extend(i)

		# Otimização do perceptron: Como o treinamento é igual
		# para todas as intancias é do conjunto escolhido faz o
		# Treinamento apenas uma vez
		if tipo_de_busca == 'PERCEPTRON':
			pesos = Perceptron.treinar(conjunto_de_treinamento_expandido, NUMERO_DE_EPOCAS_DO_PERCEPTRON)
		elif tipo_de_busca == 'AD':
			raiz = AD.constroi(conjunto_de_treinamento)

		# Itera sobre as instancias dentro do conjunto de teste
		for instancia_n_rotulada in conjunto_escolhido:

			# Aplica o algoritmo escolhido
			if tipo_de_busca == 'KNN':
				resultado = KNN.KNN(instancia_n_rotulada, K_DO_KNN, conjunto_de_treinamento_expandido)
			elif tipo_de_busca == 'PERCEPTRON':
				resultado =  Perceptron.classificar(instancia_n_rotulada, pesos)
			elif tipo_de_busca == 'AD':
				resultado = AD.classifica(instancia_n_rotulada, raiz)

			# Adiciona o resultado na matriz de confusão
			mat_confusao[instancia_n_rotulada.rotulo][resultado] += 1

	# Retorna a matriz de confusão é a taxa de acerto em %
	return mat_confusao, (float((mat_confusao[1][1] + mat_confusao[2][2])) / len(conjunto_de_treinamento)) * 100

if __name__ == '__main__':
	# Abre o arquivo e carrega pra uma lista
	conjunto_de_treinamento = []
	with open('conjuntoDeTreinamento.txt') as arq_conjunto:
		for linha in arq_conjunto.readlines():
			linha = map(lambda x: int(x), linha.split(','))
			conjunto_de_treinamento.append(Amostra(*linha))

	for tipo in ['KNN', 'PERCEPTRON', 'AD'] :
		print '='*10 + tipo + '='*10
		print '1 como 1:{0[1][1]}\n1 como 2:{0[1][2]}\n2 como 1:{0[2][1]}\n2 como 2:{0[2][2]}\nTaxa de acerto: {1}%'.format( \
			*validacao_cruzada(tipo, conjunto_de_treinamento, NUMERO_DE_DOBRAS))
